declare type Action = {
  type: string,
  payload?: any
};

declare type FetchStatus = 'loading' | 'success' | 'error';

declare type User = {
  uid: string,
  photoUrl: ?string,
  email: string,
  lastLoginAt: string,
  createdAt: string
};

declare type Todo = {
  id: string,
  description: string,
  createdAt: string
};
