// @flow
import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import middlewares from './middlewares';
import { authReducer, AUTH_MODULE_NAME } from './modules/auth';
import { todosReducer, TODOS_MODULE_NAME } from './modules/todos';

const reducer = combineReducers({
  [AUTH_MODULE_NAME]: authReducer,
  [TODOS_MODULE_NAME]: todosReducer
});
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  reducer,
  {},
  composeEnhancers(applyMiddleware(...middlewares))
);

export default store;
