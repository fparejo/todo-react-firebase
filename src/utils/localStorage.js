// @flow

export function getItem(key: string): ?any {
  const { localStorage } = window;

  return localStorage ? JSON.parse(localStorage.getItem(key)) : null;
}

export function setItem(key: string, value: any): void {
  const { localStorage } = window;

  if (localStorage) {
    localStorage.setItem(key, JSON.stringify(value));
  }
}

export function removeItem(key: string): void {
  const { localStorage } = window;

  if (localStorage) {
    localStorage.removeItem(key);
  }
}
