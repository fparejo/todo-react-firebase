// @flow
import isEmail from 'validator/lib/isEmail';
import isEmpty from 'validator/lib/isEmpty';

export function emailValidation(value: string): ?string {
  return value && isEmail(value) ? null : 'Email not valid';
}

export function requiredValidation(value: string): ?string {
  return value && !isEmpty(value) ? null : 'Field required';
}

export function passwordValidation(value: string): ?string {
  return value && !isEmpty(value) && value.length >= 6
    ? null
    : 'Password must have 6 min characters';
}
