import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

const appAPI = firebase.initializeApp({
  apiKey: 'AIzaSyBPk0yFmmgC_aZkVKYh8iDrwhiJM05qICU',
  authDomain: 'todo-react-firebase-5b942.firebaseapp.com',
  databaseURL: 'https://todo-react-firebase-5b942.firebaseio.com',
  storageBucket: 'todo-react-firebase-5b942.appspot.com'
});

export default appAPI;
