// @flow
import * as React from 'react';
import { Main } from '../../../../components';
import AppHeader from '../AppHeader/AppHeader';
import './styles.css';

type Props = {
  children: React.ChildrenArray<*>
};

function AppLayout({ children }: Props): React.Node {
  return (
    <React.Fragment>
      <AppHeader />
      <Main>{children}</Main>
    </React.Fragment>
  );
}

export default AppLayout;
