// @flow
import React from 'react';
import type { Node } from 'react';
import { Link } from 'react-router-dom';
import { Typography } from '@material-ui/core';
import { Header } from '../../../../components';
import { SignOutButton } from '../../../auth/components';
import './styles.css';

function AppHeader(): Node {
  return (
    <Header className="header">
      <Link className="header-link" to="/">
        <Typography id="header-title" variant="title" gutterBottom>
          TODO App
        </Typography>
      </Link>

      <div className="actions">
        <SignOutButton />
      </div>
    </Header>
  );
}

export default AppHeader;
