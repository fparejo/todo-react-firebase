// @flow
import * as React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { AUTH_ROUTES, AuthRouter } from '../auth';
import { TODOS_ROUTES, TodosRouter } from '../todos';
import { IsAuthenticated } from '../auth/components';
import { AppLayout } from './components';

function AppRouter(): React.Node {
  return (
    <AppLayout>
      <IsAuthenticated>
        {isAuth =>
          isAuth ? (
            <Switch>
              <Route path={TODOS_ROUTES.rootPath} component={TodosRouter} />
              <Redirect to={TODOS_ROUTES.rootPath} />
            </Switch>
          ) : (
            <Switch>
              <Route path={AUTH_ROUTES.rootPath} component={AuthRouter} />
              <Redirect to={AUTH_ROUTES.rootPath} />
            </Switch>
          )
        }
      </IsAuthenticated>
    </AppLayout>
  );
}

export default AppRouter;
