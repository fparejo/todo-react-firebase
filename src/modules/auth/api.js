// @flow
import appAPI from '../../api';

function signInWithEmail(email: string, password: string): Promise<*> {
  return appAPI.auth().signInWithEmailAndPassword(email, password);
}

function signUpWithEmail(email: string, password: string): Promise<*> {
  return appAPI.auth().createUserWithEmailAndPassword(email, password);
}

function signOut(): Promise<*> {
  return appAPI.auth().signOut();
}

function onAuthChanged(): Promise<*> {
  return new Promise((resolve, reject) => {
    appAPI.auth().onAuthStateChanged((user: User) => resolve(user), reject);
  });
}

const authAPI = {
  signInWithEmail,
  signUpWithEmail,
  signOut,
  onAuthChanged
};

export default authAPI;
