import * as selectors from './selectors';
import { initialState } from './reducer';
import { MODULE_NAME } from './constants';

describe('Auth selectors', () => {
  test('should selectUser return auth user', () => {
    const mockUser = { id: 'user' };
    const mockState = {
      [MODULE_NAME]: { ...initialState, user: mockUser }
    };

    expect(selectors.selectUser(mockState)).toEqual(mockUser);
  });

  test('should selectIsAuthenticated return true when user exists', () => {
    const mockUser = { id: 'user' };
    const mockState = {
      [MODULE_NAME]: { ...initialState, user: mockUser }
    };

    expect(selectors.selectIsAuthenticated(mockState)).toBeTruthy();
  });

  test('should selectIsAuthenticated return false when user is null', () => {
    const mockUser = null;
    const mockState = {
      [MODULE_NAME]: { ...initialState, user: mockUser }
    };

    expect(selectors.selectIsAuthenticated(mockState)).toBeFalsy();
  });
});
