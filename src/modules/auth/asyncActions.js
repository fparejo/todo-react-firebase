// @flow
import authApi from './api';
import {
  fetchAuthFail,
  fetchAuthStart,
  fetchAuthSuccess,
  setUser,
  resetUser
} from './actions';
import { setFetchFromServer } from '../todos/actions';
import type { AuthInput } from './types';
import { LOCAL_STORAGE_KEYS } from './constants';
import { setItem, removeItem } from '../../utils/localStorage';

function setAuthLocalStorage(user: User): void {
  setItem(LOCAL_STORAGE_KEYS.auth, user);
}

function resetAuthLocalStorage(): void {
  removeItem(LOCAL_STORAGE_KEYS.auth);
}

export const signIn = (credentials: AuthInput) => (dispatch: Function) => {
  dispatch(fetchAuthStart());

  authApi
    .signInWithEmail(credentials.email, credentials.password)
    .then(response => {
      const { user } = response;

      setAuthLocalStorage(user);
      dispatch(fetchAuthSuccess(user));
    })
    .catch(err => dispatch(fetchAuthFail(err.message)));
};

export const signUp = (credentials: AuthInput) => (dispatch: Function) => {
  dispatch(fetchAuthStart());

  authApi
    .signUpWithEmail(credentials.email, credentials.password)
    .then(response => {
      const { user } = response;

      setAuthLocalStorage(user);
      dispatch(fetchAuthSuccess(user));
    })
    .catch(err => dispatch(fetchAuthFail(err.message)));
};

export const signOut = () => (dispatch: Function) => {
  authApi
    .signOut()
    .then(() => {
      resetAuthLocalStorage();
      dispatch(resetUser());
      dispatch(setFetchFromServer());
    })
    .catch(err => dispatch(fetchAuthFail(err.message)));
};

export const onAuthChanged = () => (dispatch: Function) => {
  authApi
    .onAuthChanged()
    .then((user: User) => {
      if (!user) {
        return resetAuthLocalStorage();
      }

      return dispatch(setUser(user));
    })
    .catch(err => dispatch(setUser(err.message)));
};
