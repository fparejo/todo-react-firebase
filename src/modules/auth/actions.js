// @flow
import actionTypes from './actionTypes';

export function setUser(user: User): Action {
  return {
    type: actionTypes['AUTH:SET_USER'],
    payload: user
  };
}

export function resetUser(): Action {
  return {
    type: actionTypes['AUTH:RESET_USER']
  };
}

export function setError(err: string): Action {
  return {
    type: actionTypes['AUTH:SET_ERROR'],
    payload: err
  };
}

export function fetchAuthStart(): Action {
  return {
    type: actionTypes['AUTH:FETCH_START']
  };
}

export function fetchAuthSuccess(user: User): Action {
  return {
    type: actionTypes['AUTH:FETCH_SUCCESS'],
    payload: user
  };
}

export function fetchAuthFail(error: string): Action {
  return {
    type: actionTypes['AUTH:FETCH_FAIL'],
    payload: error
  };
}
