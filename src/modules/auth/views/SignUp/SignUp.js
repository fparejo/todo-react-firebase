// @flow
import React from 'react';
import type { Node } from 'react';
import { SignUpForm } from '../../components';
import { ROUTES } from '../../constants';
import './styles.css';

function SignUpView(): Node {
  return (
    <div className="sign-up-view">
      <SignUpForm
        title="Sign Up"
        buttonText="Sign Up"
        link={`${ROUTES.rootPath}${ROUTES.paths.signIn.path}`}
        linkText="I have already an account"
      />
    </div>
  );
}

export default SignUpView;
