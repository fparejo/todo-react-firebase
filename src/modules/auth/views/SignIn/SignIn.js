// @flow
import React from 'react';
import type { Node } from 'react';
import { SignInForm } from '../../components';
import { ROUTES } from '../../constants';
import './styles.css';

function SignInView(): Node {
  return (
    <div className="sign-in-view">
      <SignInForm
        title="Sign In"
        buttonText="Sign In"
        link={`${ROUTES.rootPath}${ROUTES.paths.signUp.path}`}
        linkText="Create a new account"
      />
    </div>
  );
}

export default SignInView;
