// @flow
import { getItem } from '../../utils/localStorage';
import { MODULE_NAME, LOCAL_STORAGE_KEYS } from './constants';

export function selectUser(state: Object): ?Object {
  const userFromLocalStorage = getItem(LOCAL_STORAGE_KEYS.auth);

  return userFromLocalStorage || state[MODULE_NAME].user;
}

export function selectIsAuthenticated(state: Object): boolean {
  return (
    getItem(LOCAL_STORAGE_KEYS.auth) !== null ||
    state[MODULE_NAME].user !== null
  );
}
