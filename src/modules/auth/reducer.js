// @flow
import actionTypes from './actionTypes';

type State = {
  user: ?User,
  loading: boolean,
  error: ?string
};

export const initialState: State = {
  user: null,
  loading: false,
  error: null
};

export default function authReducer(
  state: State = initialState,
  action: Action
) {
  switch (action.type) {
    case actionTypes['AUTH:SET_USER']:
      return {
        ...state,
        user: action.payload
      };
    case actionTypes['AUTH:RESET_USER']:
      return {
        ...state,
        user: null
      };
    case actionTypes['AUTH:FETCH_START']:
      return {
        ...state,
        loading: true,
        error: null
      };
    case actionTypes['AUTH:FETCH_SUCCESS']:
      return {
        ...state,
        user: action.payload,
        loading: false,
        error: null
      };
    case actionTypes['AUTH:FETCH_FAIL']:
      return {
        ...state,
        loading: false,
        error: action.payload
      };
    case actionTypes['AUTH:SET_ERROR']:
      return {
        ...state,
        error: action.payload
      };
    default:
      return state;
  }
}
