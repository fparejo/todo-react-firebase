// @flow
import { connect } from 'react-redux';
import { signIn } from '../../asyncActions';
import AuthFormComponent from '../AuthForm/AuthForm';

const mapDispatchToProps = {
  onSubmit: signIn
};

const SignInFormContainer = connect(
  null,
  mapDispatchToProps
)(AuthFormComponent);

SignInFormContainer.displayName = 'SignInFormContainer';

export default SignInFormContainer;
