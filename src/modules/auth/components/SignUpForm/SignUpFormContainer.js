// @flow
import { connect } from 'react-redux';
import { signUp } from '../../asyncActions';
import AuthFormComponent from '../AuthForm/AuthForm';

const mapDispatchToProps = {
  onSubmit: signUp
};

const SignUpFormContainer = connect(
  null,
  mapDispatchToProps
)(AuthFormComponent);

SignUpFormContainer.displayName = 'SignUpFormContainer';

export default SignUpFormContainer;
