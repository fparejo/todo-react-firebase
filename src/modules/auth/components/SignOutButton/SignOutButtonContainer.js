// @flow
import { connect } from 'react-redux';
import SignOutButtonComponent from './SignOutButton';
import { signOut } from '../../asyncActions';
import { selectIsAuthenticated } from '../../selectors';

type MapStateToProps = {
  isAuthenticated: boolean
};

type MapDispatchToProps = {
  signOut: Function
};

const mapDispatchToProps: MapDispatchToProps = {
  signOut
};

function mapStateToProps(state: Object): MapStateToProps {
  return {
    isAuthenticated: selectIsAuthenticated(state)
  };
}

const SignOutButtonContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(SignOutButtonComponent);

SignOutButtonContainer.displayName = 'SignOutButtonContainer';

export default SignOutButtonContainer;
