// @flow
import React from 'react';
import type { Node } from 'react';
import { IconButton } from '@material-ui/core';
import { PowerSettingsNew } from '@material-ui/icons';
import './styles.css';

type Props = {
  signOut: Function,
  isAuthenticated: boolean
};

function SignOutButton({ signOut, isAuthenticated }: Props): Node {
  if (!isAuthenticated) {
    return null;
  }

  return (
    <IconButton
      className="sign-out-icon"
      aria-label="Sign Out"
      onClick={signOut}
    >
      <PowerSettingsNew color="action" />
    </IconButton>
  );
}

export default SignOutButton;
