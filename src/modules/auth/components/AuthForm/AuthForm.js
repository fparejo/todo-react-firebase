// @flow
import React from 'react';
import type { Node } from 'react';
import { Form } from 'react-form';
import { Link } from 'react-router-dom';
import { Button, Paper, Typography } from '@material-ui/core';
import { TextField } from '../../../../components/Forms';
import {
  emailValidation,
  passwordValidation
} from '../../../../utils/validations';
import './styles.css';

type Props = {
  title: string,
  buttonText: string,
  link: string,
  linkText: string,
  onSubmit: Function
};

function AuthForm({
  title,
  buttonText,
  link,
  linkText,
  onSubmit
}: Props): Node {
  return (
    <Form onSubmit={onSubmit}>
      {formApi => (
        <Paper className="auth-form">
          <form onSubmit={formApi.submitForm}>
            <Typography variant="title" gutterBottom>
              {title}
            </Typography>
            <div className="fields-wrapper">
              <TextField
                id="email"
                type="email"
                field="email"
                label="Email"
                validate={emailValidation}
              />
              <TextField
                id="password"
                type="password"
                field="password"
                label="Password"
                validate={passwordValidation}
              />
            </div>

            <Button
              className="confirm-button"
              size="medium"
              type="submit"
              color="primary"
              variant="raised"
            >
              {buttonText}
            </Button>
          </form>

          <Link to={link}>{linkText}</Link>
        </Paper>
      )}
    </Form>
  );
}

export default AuthForm;
