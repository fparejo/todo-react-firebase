import React from 'react';
import { shallow } from 'enzyme';
import { Form } from 'react-form';
import { Link } from 'react-router-dom';
import { Button, Typography } from '@material-ui/core';
import { TextField } from '../../../../components/Forms';
import {
  emailValidation,
  passwordValidation
} from '../../../../utils/validations';
import AuthForm from './AuthForm';

const mockFormApi = {
  submitForm: jest.fn()
};

describe('<AuthForm />', () => {
  test('should render the component', () => {
    const wrapper = shallow(<AuthForm />);
    expect(wrapper.exists()).toBeTruthy();
  });

  test('should render value of props.title', () => {
    const mockTitle = 'Test';
    const wrapper = shallow(<AuthForm title={mockTitle} link="link" />);
    const formWrapper = wrapper.find(Form);

    const children = shallow(formWrapper.props().children(mockFormApi));
    const titleWrapper = children.find(Typography);

    expect(titleWrapper.props().children).toBe(mockTitle);
  });

  test('should render a email field', () => {
    const mockTitle = 'Test';
    const wrapper = shallow(<AuthForm title={mockTitle} link="link" />);
    const formWrapper = wrapper.find(Form);

    const children = shallow(formWrapper.props().children(mockFormApi));
    const inputWrapper = children.find(TextField).find('#email');

    expect(inputWrapper.props()).toEqual({
      id: 'email',
      type: 'email',
      field: 'email',
      label: 'Email',
      validate: emailValidation
    });
  });

  test('should render a password field', () => {
    const mockTitle = 'Test';
    const wrapper = shallow(<AuthForm title={mockTitle} link="link" />);
    const formWrapper = wrapper.find(Form);

    const children = shallow(formWrapper.props().children(mockFormApi));
    const inputWrapper = children.find(TextField).find('#password');

    expect(inputWrapper.props()).toEqual({
      id: 'password',
      type: 'password',
      field: 'password',
      label: 'Password',
      validate: passwordValidation
    });
  });

  test('should render a button with props.buttonText value', () => {
    const mockButtonText = 'Button';
    const wrapper = shallow(
      <AuthForm link="link" buttonText={mockButtonText} />
    );
    const formWrapper = wrapper.find(Form);

    const children = shallow(formWrapper.props().children(mockFormApi));
    const buttonWrapper = children.find(Button);

    expect(buttonWrapper.props()).toEqual({
      className: 'confirm-button',
      size: 'medium',
      type: 'submit',
      color: 'primary',
      variant: 'raised',
      children: mockButtonText
    });
  });

  test('should render a Link with props.link and props.linkText values', () => {
    const mockLink = {
      text: 'Link',
      link: 'http://www.google.es'
    };
    const wrapper = shallow(
      <AuthForm link={mockLink.link} linkText={mockLink.text} />
    );
    const formWrapper = wrapper.find(Form);

    const children = shallow(formWrapper.props().children(mockFormApi));
    const linkWrapper = children.find(Link);

    expect(linkWrapper.props()).toEqual({
      to: mockLink.link,
      children: mockLink.text,
      replace: false
    });
  });
});
