// @flow
import { Component } from 'react';
import { connect } from 'react-redux';
import { selectIsAuthenticated } from '../../selectors';
import { onAuthChanged as authChangeAction } from '../../asyncActions';

type MapStateToProps = {
  isAuthenticated: boolean
};

type MapDispatchToProps = {
  onAuthChanged: Function
};

type Props = MapStateToProps & MapDispatchToProps & { children: Function };

class InnerComponent extends Component<Props> {
  componentDidMount() {
    const { onAuthChanged } = this.props;
    onAuthChanged();
  }

  render() {
    const { children, isAuthenticated } = this.props;
    return children(isAuthenticated);
  }
}

const mapDispatchToProps: MapDispatchToProps = {
  onAuthChanged: authChangeAction
};

function mapStateToProps(state: Object): MapStateToProps {
  return {
    isAuthenticated: selectIsAuthenticated(state)
  };
}

const IsAuthenticatedContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(InnerComponent);

IsAuthenticatedContainer.displayName = 'IsAuthenticatedContainer';

export default IsAuthenticatedContainer;
