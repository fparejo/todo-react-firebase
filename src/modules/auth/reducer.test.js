import * as actions from './actions';
import reducer, { initialState } from './reducer';

describe('Auth reducer', () => {
  test('should AUTH:SET_USER create a new state', () => {
    const mockUser = { id: 'user' };
    const state = reducer(initialState, actions.setUser(mockUser));

    expect(state).toEqual({ ...initialState, user: mockUser });
  });

  test('should AUTH:RESET_USER create a new state', () => {
    const state = reducer(initialState, actions.resetUser());

    expect(state).toEqual({ ...initialState, user: null });
  });

  test('should AUTH:FETCH_START create a new state', () => {
    const state = reducer(initialState, actions.fetchAuthStart());

    expect(state).toEqual({ ...initialState, loading: true });
  });

  test('should AUTH:FETCH_SUCCESS create a new state', () => {
    const mockUser = { id: 'user' };
    const state = reducer(initialState, actions.fetchAuthSuccess(mockUser));

    expect(state).toEqual({ ...initialState, user: mockUser, loading: false });
  });

  test('should AUTH:FETCH_FAIL create a new state', () => {
    const mockError = 'error';
    const state = reducer(initialState, actions.fetchAuthFail(mockError));

    expect(state).toEqual({ ...initialState, error: mockError });
  });

  test('should AUTH:SET_ERROR create a new state', () => {
    const mockError = 'error';
    const state = reducer(initialState, actions.setError(mockError));

    expect(state).toEqual({ ...initialState, error: mockError });
  });
});
