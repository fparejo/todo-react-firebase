// @flow
import * as React from 'react';
import { Switch, Route } from 'react-router-dom';
import { SignInView, SignUpView } from './views';
import { ROUTES } from './constants';

function AuthRouter({ match }: Route): React.Node {
  return (
    <Switch>
      <Route
        exact
        path={`${match.path}${ROUTES.paths.signIn.path}`}
        component={SignInView}
      />
      <Route
        exact
        path={`${match.path}${ROUTES.paths.signUp.path}`}
        component={SignUpView}
      />
    </Switch>
  );
}

export default AuthRouter;
