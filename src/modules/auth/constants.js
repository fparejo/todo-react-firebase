export const MODULE_NAME = 'auth';

export const ROUTES = {
  rootPath: '/auth',
  paths: {
    signIn: {
      id: 'signIn',
      title: 'Sign In',
      path: '/'
    },
    signUp: {
      id: 'signUp',
      title: 'Sign Up',
      path: '/sign-up'
    }
  }
};

export const LOCAL_STORAGE_KEYS = {
  auth: 'auth'
};
