import * as actions from './actions';

describe('Auth actions', () => {
  test('should setUser return an action', () => {
    const action = actions.setUser({});

    expect(action).toMatchSnapshot();
  });

  test('should resetUser return an action', () => {
    const action = actions.resetUser([]);

    expect(action).toMatchSnapshot();
  });

  test('should setError return an action', () => {
    const action = actions.setError('error');

    expect(action).toMatchSnapshot();
  });

  test('should fetchAuthStart return an action', () => {
    const action = actions.fetchAuthStart();

    expect(action).toMatchSnapshot();
  });

  test('should fetchAuthSuccess return an action', () => {
    const action = actions.fetchAuthSuccess({});

    expect(action).toMatchSnapshot();
  });

  test('should fetchAuthFail return an action', () => {
    const action = actions.fetchAuthFail({});

    expect(action).toMatchSnapshot();
  });
});
