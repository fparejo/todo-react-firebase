// @flow

export type TodoFormData = {
  description: string
};
