// @flow
import actionTypes from './actionTypes';
import type { TodoFormData } from './types';

export function setTodosData(todos: Array<Todo>): Action {
  return {
    type: actionTypes['TODOS:SET_DATA'],
    payload: todos
  };
}

export function addTodo(todo: Todo): Action {
  return {
    type: actionTypes['TODOS:ADD_TODO'],
    payload: todo
  };
}

export function removeTodo(index: number): Action {
  return {
    type: actionTypes['TODOS:REMOVE_TODO'],
    payload: index
  };
}

export function updateTodo(todo: TodoFormData, index: number): Action {
  return {
    type: actionTypes['TODOS:UPDATE_TODO'],
    payload: {
      todo,
      index
    }
  };
}

export function setFetchFromServer(): Action {
  return {
    type: actionTypes['TODOS:SET_FETCH_FROM_SERVER']
  };
}

export function resetFetchFromServer(): Action {
  return {
    type: actionTypes['TODOS:RESET_FETCH_FROM_SERVER']
  };
}

export function fetchTodosStart(): Action {
  return {
    type: actionTypes['TODOS:FETCH_TODOS_START']
  };
}

export function fetchTodosSuccess(): Action {
  return {
    type: actionTypes['TODOS:FETCH_TODOS_SUCCESS']
  };
}

export function fetchTodosFail(err: string): Action {
  return {
    type: actionTypes['TODOS:FETCH_TODOS_FAIL'],
    payload: err
  };
}
