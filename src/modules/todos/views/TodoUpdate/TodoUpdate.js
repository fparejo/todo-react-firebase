// @flow
import React from 'react';
import type { Node } from 'react';
import { TodoUpdateForm } from '../../components';
import './styles.css';

function TodoUpdateView(): Node {
  return (
    <div className="todo-create-view">
      <TodoUpdateForm />
    </div>
  );
}

export default TodoUpdateView;
