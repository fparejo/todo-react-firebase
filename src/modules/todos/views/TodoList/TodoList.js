// @flow
import React from 'react';
import type { Node } from 'react';
import { Link } from 'react-router-dom';
import { Button } from '@material-ui/core';
import { Add } from '@material-ui/icons';
import { TodoList } from '../../components';
import { ROUTES } from '../../constants';
import './styles.css';

function TodoListView(): Node {
  return (
    <div className="todo-list-view">
      <TodoList />

      <Link
        className="add-button"
        to={`${ROUTES.rootPath}${ROUTES.paths.todoCreate.path}`}
      >
        <Button variant="fab" color="primary" aria-label="Add">
          <Add />
        </Button>
      </Link>
    </div>
  );
}

export default TodoListView;
