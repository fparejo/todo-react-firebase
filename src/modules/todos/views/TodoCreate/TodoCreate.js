// @flow
import React from 'react';
import type { Node } from 'react';
import { TodoCreateForm } from '../../components';
import './styles.css';

function TodoCreateView(): Node {
  return (
    <div className="todo-create-view">
      <TodoCreateForm />
    </div>
  );
}

export default TodoCreateView;
