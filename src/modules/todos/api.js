// @flow
import appAPI from '../../api';
import type { TodoFormData } from './types';

const database = appAPI.database();

function getTodos(userId: string): Promise<*> {
  return database.ref(`/todos/${userId}`).once('value');
}

function getTodo(todoId: string, userId: string): Promise<*> {
  return database.ref(`/todos/${userId}/${todoId}`).once('value');
}

function createTodo(newTodo: TodoFormData, userId: string): Promise<*> {
  return database
    .ref(`/todos/${userId}`)
    .push({ ...newTodo, createdAt: new Date(Date.now()).toLocaleString() });
}

function updateTodo(
  modifiedTodo: TodoFormData,
  todoId: string,
  userId: string
): Promise<*> {
  return database.ref(`/todos/${userId}/${todoId}`).update(modifiedTodo);
}

function removeTodo(todoId: string, userId: string): Promise<*> {
  return database.ref(`/todos/${userId}/${todoId}`).remove();
}

const todoAPI = {
  getTodos,
  getTodo,
  createTodo,
  updateTodo,
  removeTodo
};

export default todoAPI;
