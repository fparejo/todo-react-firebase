export const MODULE_NAME = 'todos';

export const ROUTES = {
  rootPath: '/todos',
  paths: {
    todosList: {
      id: 'todosList',
      title: 'Todos List',
      path: '/'
    },
    todoCreate: {
      id: 'todoCreate',
      title: 'New Todo',
      path: '/create'
    },
    todoUpdate: {
      id: 'todoUpdate',
      title: 'Edit Todo',
      path: '/:todoId'
    }
  }
};
