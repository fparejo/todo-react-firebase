// @flow
import React from 'react';
import type { Node } from 'react';
import { CircularProgress, Typography } from '@material-ui/core';
import TodoItem from '../TodoItem/TodoItemContainer';
import './styles.css';

type Props = {
  todos: Array<Todo>,
  loading: boolean
};

function TodoList({ todos, loading }: Props): Node {
  if (loading) {
    return <CircularProgress />;
  }

  if (todos.length === 0) {
    return (
      <Typography variant="body1" gutterBottom>
        There are not todos items yet.
      </Typography>
    );
  }

  return (
    <div className="todo-list">
      {todos.map(todo => (
        <TodoItem key={todo.id} todo={todo} />
      ))}
    </div>
  );
}

export default TodoList;
