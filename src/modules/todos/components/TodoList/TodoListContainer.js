// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { postTodo, fetchTodos } from '../../asyncActions';
import {
  selectTodosData,
  selectTodosLoading,
  selectFetchFromServer
} from '../../selectors';
import TodoListComponent from './TodoList';

type MapDispatchToProps = {
  fetchTodos: Function,
  postTodo: Function
};

type MapStateToProps = {
  todos: Array<Todo>,
  fetchFromServer: boolean,
  loading: boolean
};

type Props = MapDispatchToProps & MapStateToProps;

class InnerComponent extends Component<Props> {
  componentDidMount() {
    const { fetchFromServer } = this.props;

    if (fetchFromServer) {
      this.props.fetchTodos();
    }
  }

  render() {
    return <TodoListComponent {...this.props} />;
  }
}

const mapDispatchToProps: MapDispatchToProps = {
  fetchTodos,
  postTodo
};

function mapStateTopProps(state: Object): MapStateToProps {
  return {
    todos: selectTodosData(state),
    fetchFromServer: selectFetchFromServer(state),
    loading: selectTodosLoading(state)
  };
}

const TodoListContainer = connect(
  mapStateTopProps,
  mapDispatchToProps
)(InnerComponent);

TodoListContainer.displayName = 'TodoListContainer';

export default TodoListContainer;
