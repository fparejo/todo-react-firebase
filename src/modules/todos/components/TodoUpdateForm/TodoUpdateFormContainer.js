// @flow
import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { putTodo, fetchTodos as fetchTodosAction } from '../../asyncActions';
import { selectTodo, selectTodosLoading } from '../../selectors';
import TodoFormComponent from '../TodoForm/TodoForm';

type MapDispatchToProps = {
  fetchTodos: Function,
  onSubmit: Function
};

type MapStateToProps = {
  todo: ?Todo,
  loading: boolean
};

type OwnProps = {
  match: {
    params: {
      todoId: string
    }
  }
};

type Props = MapDispatchToProps & MapStateToProps & OwnProps;

class InnerComponent extends Component<Props> {
  componentDidMount() {
    const { todo, fetchTodos } = this.props;

    if (!todo) {
      fetchTodos();
    }
  }

  render() {
    return <TodoFormComponent {...this.props} />;
  }
}

function mapDispatchToProps(
  dispatch: Function,
  { match }: OwnProps
): MapDispatchToProps {
  return {
    fetchTodos() {
      dispatch(fetchTodosAction());
    },
    onSubmit(newTodo) {
      dispatch(putTodo(newTodo, match.params.todoId));
    }
  };
}

function mapStateToProps(state: Object, { match }: OwnProps): MapStateToProps {
  return {
    todo: selectTodo(state, match.params.todoId),
    loading: selectTodosLoading(state)
  };
}

const TodoUpdateFormContainer = compose(
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(InnerComponent);

TodoUpdateFormContainer.displayName = 'TodoUpdateFormContainer';

export default TodoUpdateFormContainer;
