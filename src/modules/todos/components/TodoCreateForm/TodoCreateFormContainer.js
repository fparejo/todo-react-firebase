// @flow
import { connect } from 'react-redux';
import { postTodo } from '../../asyncActions';
import TodoFormComponent from '../TodoForm/TodoForm';

type MapDispatchToProps = {
  onSubmit: Function
};

const mapDispatchToProps: MapDispatchToProps = {
  onSubmit: postTodo
};

const TodoCreateFormContainer = connect(
  null,
  mapDispatchToProps
)(TodoFormComponent);

TodoCreateFormContainer.displayName = 'TodoCreateFormContainer';

export default TodoCreateFormContainer;
