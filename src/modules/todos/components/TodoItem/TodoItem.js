// @flow
import React from 'react';
import type { Node } from 'react';
import { Link } from 'react-router-dom';
import {
  Card,
  CardContent,
  CardActions,
  IconButton,
  Typography
} from '@material-ui/core';
import { DeleteOutline, Edit } from '@material-ui/icons';
import { ROUTES } from '../../constants';
import './styles.css';

type Props = {
  todo: Todo,
  removeTodo: Function
};

function TodoItem({ todo, removeTodo }: Props): Node {
  return (
    <Card className="todo-item">
      <CardContent>
        <Typography variant="title" gutterBottom>
          {todo.description}
        </Typography>

        <Typography variant="caption" gutterBottom align="left">
          {`CREATED AT ${todo.createdAt}`}
        </Typography>
      </CardContent>
      <CardActions>
        <Link to={`${ROUTES.rootPath}/${todo.id}`}>
          <IconButton color="primary" aria-label="Edit Todo">
            <Edit />
          </IconButton>
        </Link>
        <IconButton
          color="primary"
          aria-label="Remove Todo"
          onClick={removeTodo}
        >
          <DeleteOutline />
        </IconButton>
      </CardActions>
    </Card>
  );
}

export default TodoItem;
