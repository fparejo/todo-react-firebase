// @flow
import { connect } from 'react-redux';
import { deleteTodo } from '../../asyncActions';
import TodoItemComponent from './TodoItem';

type MapDispathToProps = {
  removeTodo: Function
};

type OwnProps = {
  todo: Todo
};

function mapDispatchToProps(
  dispatch: Function,
  { todo }: OwnProps
): MapDispathToProps {
  return {
    removeTodo() {
      dispatch(deleteTodo(todo.id));
    }
  };
}

const TodoItemContainer = connect(
  null,
  mapDispatchToProps
)(TodoItemComponent);

TodoItemContainer.displayName = 'TodoItemComponent';

export default TodoItemContainer;
