// @flow
import React from 'react';
import type { Node } from 'react';
import { Form } from 'react-form';
import { Button, Paper, Typography, CircularProgress } from '@material-ui/core';
import { TextField } from '../../../../components/Forms';
import { requiredValidation } from '../../../../utils/validations';
import './styles.css';

type Props = {
  todo: ?Todo,
  loading: boolean,
  onSubmit: Function
};

function TodoForm({ todo, loading, onSubmit }: Props): Node {
  if (loading) {
    return <CircularProgress />;
  }

  return (
    <Form
      onSubmit={onSubmit}
      values={{ description: todo ? todo.description : '' }}
    >
      {formApi => (
        <Paper className="todo-form">
          <form onSubmit={formApi.submitForm}>
            <Typography variant="title" gutterBottom>
              {todo ? 'Edit Todo' : 'Create Todo'}
            </Typography>
            <div className="fields-wrapper">
              <TextField
                id="todo-description"
                type="text"
                field="description"
                label="Description"
                validate={requiredValidation}
              />
            </div>

            <Button
              className="confirm-button"
              size="medium"
              type="submit"
              color="primary"
              variant="raised"
            >
              {todo ? 'Edit' : 'Create'}
            </Button>
          </form>
        </Paper>
      )}
    </Form>
  );
}

export default TodoForm;
