// @flow
import * as React from 'react';
import { Switch, Route } from 'react-router-dom';
import { TodoCreate, TodoList, TodoUpdate } from './views';
import { ROUTES } from './constants';

function TodosRouter({ match }: Route): React.Node {
  return (
    <Switch>
      <Route
        exact
        path={`${match.path}${ROUTES.paths.todosList.path}`}
        component={TodoList}
      />
      <Route
        exact
        path={`${match.path}${ROUTES.paths.todoCreate.path}`}
        component={TodoCreate}
      />
      <Route
        exact
        path={`${match.path}${ROUTES.paths.todoUpdate.path}`}
        component={TodoUpdate}
      />
    </Switch>
  );
}

export default TodosRouter;
