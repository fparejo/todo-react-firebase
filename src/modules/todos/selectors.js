// @flow
import { MODULE_NAME } from './constants';

export function selectTodosData(state: Object): Array<Todo> {
  return state[MODULE_NAME].data;
}

export function selectTodosLoading(state: Object): boolean {
  return state[MODULE_NAME].loading;
}

export function selectTodo(state: Object, todoId: string): ?Todo {
  return state[MODULE_NAME].data.find(todo => todo.id === todoId);
}

export function selectIndexTodo(state: Object, todoId: string): number {
  return state[MODULE_NAME].data.findIndex(todo => todo.id === todoId);
}

export function selectFetchFromServer(state: Object): boolean {
  return state[MODULE_NAME].fetchFromServer;
}
