// @flow
import actionTypes from './actionTypes';

type State = {
  data: Array<Todo>,
  loading: false,
  error: null,
  fetchFromServer: boolean
};

export const initialState: State = {
  data: [],
  loading: false,
  error: null,
  fetchFromServer: true
};

export default function todosReducer(
  state: State = initialState,
  action: Action
) {
  switch (action.type) {
    case actionTypes['TODOS:SET_DATA']:
      return {
        ...state,
        data: action.payload
      };
    case actionTypes['TODOS:ADD_TODO']:
      return {
        ...state,
        data: [...state.data, action.payload]
      };
    case actionTypes['TODOS:REMOVE_TODO']:
      return {
        ...state,
        data: [
          ...state.data.slice(0, action.payload),
          ...state.data.slice(action.payload + 1)
        ]
      };
    case actionTypes['TODOS:UPDATE_TODO']:
      return {
        ...state,
        data: [
          ...state.data.slice(0, action.payload.index),
          { ...state.data[action.payload.index], ...action.payload.todo },
          ...state.data.slice(action.payload.index + 1)
        ]
      };
    case actionTypes['TODOS:FETCH_TODOS_START']:
      return {
        ...state,
        loading: true,
        error: null
      };
    case actionTypes['TODOS:FETCH_TODOS_SUCCESS']:
      return {
        ...state,
        loading: false,
        error: null
      };
    case actionTypes['TODOS:FETCH_TODOS_FAIL']:
      return {
        ...state,
        loading: false,
        error: action.payload
      };
    case actionTypes['TODOS:SET_FETCH_FROM_SERVER']:
      return {
        ...state,
        fetchFromServer: true
      };
    case actionTypes['TODOS:RESET_FETCH_FROM_SERVER']:
      return {
        ...state,
        fetchFromServer: false
      };
    default:
      return state;
  }
}
