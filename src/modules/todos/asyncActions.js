// @flow
import todoAPI from './api';
import type { TodoFormData } from './types';
import { selectUser } from '../auth/selectors';
import { selectIndexTodo } from './selectors';
import {
  setTodosData,
  addTodo,
  removeTodo,
  updateTodo,
  fetchTodosStart,
  fetchTodosSuccess,
  fetchTodosFail,
  resetFetchFromServer
} from './actions';

export const fetchTodos = () => (dispatch: Function, getState: Function) => {
  const user = selectUser(getState());

  dispatch(fetchTodosStart());

  if (!user) {
    fetchTodosFail('Todo user list not found');
    return;
  }

  todoAPI
    .getTodos(user.uid)
    .then(snapshot => {
      const todosMap = snapshot.val() || {};
      const todos = Object.keys(todosMap).map(todoId => ({
        ...todosMap[todoId],
        id: todoId
      }));

      dispatch(fetchTodosSuccess());
      dispatch(setTodosData(todos));
      dispatch(resetFetchFromServer());
    })
    .catch(err => dispatch(fetchTodosFail(err.message)));
};

export const postTodo = (newTodo: TodoFormData) => (
  dispatch: Function,
  getState: Function
) => {
  const user = selectUser(getState());

  dispatch(fetchTodosStart());

  todoAPI
    .createTodo(newTodo, user.uid)
    .then(ref => todoAPI.getTodo(ref.key, user.uid))
    .then(snapshot => {
      const todo: Todo = snapshot.val();

      dispatch(addTodo({ ...todo, id: snapshot.key }));
      dispatch(fetchTodosSuccess());
    })
    .catch(err => dispatch(fetchTodosFail(err.message)));
};

export const putTodo = (modifiedTodo: TodoFormData, todoId: string) => (
  dispatch: Function,
  getState: Function
) => {
  const user = selectUser(getState());

  dispatch(fetchTodosStart());

  todoAPI
    .updateTodo(modifiedTodo, todoId, user.uid)
    .then(() => {
      const todoIndex = selectIndexTodo(getState(), todoId);
      dispatch(updateTodo({ ...modifiedTodo, id: todoId }, todoIndex));
      dispatch(fetchTodosSuccess());
    })
    .catch(err => dispatch(fetchTodosFail(err.message)));
};

export const deleteTodo = (todoId: string) => (
  dispatch: Function,
  getState: Function
) => {
  const user = selectUser(getState());

  dispatch(fetchTodosStart());

  todoAPI
    .removeTodo(todoId, user.uid)
    .then(() => {
      const todoIndex = selectIndexTodo(getState(), todoId);

      dispatch(fetchTodosSuccess());
      dispatch(removeTodo(todoIndex));
    })
    .catch(err => dispatch(fetchTodosFail(err.message)));
};
