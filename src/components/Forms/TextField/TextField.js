// @flow
import * as React from 'react';
import Field from 'react-form/lib/components/Field';
import { TextField as MaterialTextField } from '@material-ui/core';

type Props = {
  type: 'text' | 'email' | 'password' | 'number',
  id: string,
  label: string,
  field: string,
  validate?: Function
};

function TextField({ validate, field, ...props }: Props) {
  return (
    <Field validate={validate} field={field}>
      {(fieldApi: Object) => (
        <MaterialTextField
          value={fieldApi.value || ''}
          error={Boolean(fieldApi.error)}
          name={fieldApi.fieldName}
          helperText={fieldApi.error}
          onChange={evt => fieldApi.setValue(evt.target.value)}
          onBlur={() => fieldApi.setTouched()}
          {...props}
        />
      )}
    </Field>
  );
}

TextField.defaultProps = {
  validate: null
};

export default TextField;
