// @flow
import * as React from 'react';
import './styles.css';

type Props = {
  children: React.Node
};

function Main({ children }: Props): React.Node {
  return (
    <main className="main">
      <div className="content">{children}</div>
    </main>
  );
}

export default Main;
