// @flow
import React from 'react';
import type { ChildrenArray, Node } from 'react';
import AppBar from '@material-ui/core/AppBar';
import './styles.css';

type Props = {
  children: ChildrenArray<*>,
  className: string
};

function Header({ children, className }: Props): Node {
  return (
    <AppBar className={className} id="header">
      {children}
    </AppBar>
  );
}

export default Header;
